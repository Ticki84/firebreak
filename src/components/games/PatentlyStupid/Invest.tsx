import { Button, Grid, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import { Invention } from './Invent';

type PatentlyStupidInventProps = {
  sendPlayerInvestment: (investment: Investment) => Promise<void>;
  currentProblems: Record<string, string>;
  currentInventions: Record<string, Invention>;
  currentInvestments: Record<string, Investment>;
  userUid: string;
};

export type Investment = {
  bigStack: string;
  mediumStack: string;
  smallStack: string;
};

const Invest: React.FC<PatentlyStupidInventProps> = ({
  sendPlayerInvestment: sendInvestment,
  currentProblems,
  currentInventions,
  currentInvestments,
  userUid,
}) => {
  const [validated, setValidated] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [investment, setInvestment] = useState({
    bigStack: '',
    mediumStack: '',
    smallStack: '',
  });

  const onValidateInvestment = async () => {
    if (
      (investment.bigStack.length !== 0 ? 1 : 0) +
        (investment.mediumStack.length !== 0 ? 1 : 0) +
        (investment.smallStack.length !== 0 ? 1 : 0) <
      Math.min(Object.keys(currentInventions).length - 1, 3)
    )
      return;
    setValidated(true);
    await sendInvestment(investment);
  };

  const getValidations = () => {
    const investmentsEntries =
      currentInvestments! &&
      Object.entries(currentInvestments).map(([uid, investments]) => (
        <Grid item key={uid}>
          {investments ? (
            <CheckCircleIcon fontSize="large" htmlColor="green" />
          ) : (
            <CancelIcon fontSize="large" htmlColor="red" />
          )}
        </Grid>
      ));
    return (
      <>
        <Grid item xs={12}>
          <Typography variant="h4" align="center">
            You&apos;ve wisely invested your money!
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h4" align="center">
            Count your money while the other players are still investing...
          </Typography>
        </Grid>
        <Grid
          container
          alignItems="center"
          justify="center"
          spacing={4}
          xs={12}
        >
          {investmentsEntries}
        </Grid>
      </>
    );
  };

  const stack = (inventorUid: string, size: string) => (
    <svg width="128" height="128" viewBox="0 0 1200 1200">
      <defs>
        <linearGradient id="StackGradient" x1="0" y1="0" x2="1" y2="1">
          <stop offset="0%" stopColor="#f12711" />
          <stop offset="100%" stopColor="#f5af19" />
        </linearGradient>
      </defs>

      {((investment.bigStack === inventorUid && size === 'big') ||
        (investment.mediumStack === inventorUid && size === 'medium') ||
        (investment.smallStack === inventorUid && size === 'small')) && (
        <svg viewBox="0 0 99.999997 99.999997">
          <g transform="translate(-149.6453,-589.21878)">
            <path
              d="m 199.64552,591.21879 a 48.000099,48.000099 0 0 0 -48.00022,48.00022 48.000099,48.000099 0 0 0 48.00022,47.99977 48.000099,48.000099 0 0 0 47.99977,-47.99977 48.000099,48.000099 0 0 0 -47.99977,-48.00022 z m 0,5.42088 a 42.107528,42.578935 0 0 1 42.10718,42.57934 42.107528,42.578935 0 0 1 -42.10718,42.57887 42.107528,42.578935 0 0 1 -42.10763,-42.57887 42.107528,42.578935 0 0 1 42.10763,-42.57934 z"
              fill="url(#StackGradient)"
            />
          </g>
        </svg>
      )}
      <g transform="translate(600 600) scale(0.8 0.8) rotate(0) translate(-600 -600)">
        <svg
          version="1.1"
          x="0px"
          y="0px"
          viewBox="0 0 100 100"
          enableBackground="new 0 0 100 100"
        >
          {size === 'big' && (
            <path
              fill="#3BB300"
              d="M93.271,39.076L68.459,25.217l-7.111-3.953L36.536,7.436c-0.106-0.06-0.224-0.089-0.341-0.089  c-0.125,0-0.249,0.033-0.359,0.1L7.742,24.243c-0.211,0.126-0.341,0.354-0.341,0.601v5.535L6.89,30.676  c-0.215,0.126-0.348,0.356-0.348,0.605v7.266c0,0.251,0.135,0.483,0.353,0.607l0.522,0.299v5.696l-0.549,0.327  c-0.212,0.127-0.342,0.355-0.342,0.602l0.002,3.634c0.001,0.908,0.003,1.817,0.005,2.499c0.001,0.493,0.003,0.861,0.03,1.133H6.542  v6.906c0,0.25,0.134,0.481,0.35,0.606L31.298,74.95l6.983,4.046l24.392,14.095c0.107,0.062,0.229,0.094,0.35,0.094  s0.242-0.031,0.35-0.094l28.578-16.516c0.217-0.125,0.351-0.356,0.351-0.606v-7.13l0.106-0.063c0.215-0.125,0.347-0.355,0.347-0.604  v-5.528l0.101-0.061c0.209-0.127,0.337-0.354,0.337-0.599V54.75c0-0.246-0.13-0.475-0.342-0.602l-0.549-0.327v-5.706l0.988-0.593  c0.211-0.127,0.34-0.354,0.34-0.601v-7.234C93.63,39.434,93.492,39.2,93.271,39.076z M92.63,46.752l-1.181,0.709  c0,0-0.001,0-0.002,0L65.151,63.189v-6.534L92.63,40.217V46.752z M91.755,68.001l-0.54,0.319c0,0-0.001,0-0.002,0l-13.875,8.297  l-13.077,7.808v-5.799L91.755,62.77V68.001z M91.302,75.796L63.323,91.965v-6.566l0.413-0.22l0.098,0.046  c0.041,0.02,0.084,0.028,0.127,0.028l0,0l0,0c0.053,0,0.105-0.014,0.154-0.042l13.531-8.078l0.015-0.009l13.641-7.886V75.796z   M36.201,8.394l24.207,13.49L32.924,38.341L8.701,24.835L36.201,8.394z M91.302,54.045L63.323,70.229v-6.568l0.403-0.224  l0.979,0.544c0.046,0.025,0.096,0.037,0.146,0.037l0,0l0,0l0,0l0,0c0.014,0,0.025-0.009,0.039-0.011  c0.039-0.005,0.078-0.011,0.111-0.03c0.002-0.001,0.002,0,0.004-0.001l26.296-15.729V54.045z M63.029,84.876l-3.886-2.245  l3.957,2.207L63.029,84.876z M23.533,54.834L23.533,54.834L8.417,46.108v-0.562v-5.981l23.075,13.3c0,0,0,0.001,0,0.001l0.731,0.443  v6.551l-0.018-0.01c0,0,0-0.001,0-0.001L23.533,54.834z M63.872,62.831C63.871,62.831,63.871,62.831,63.872,62.831L40.323,49.715  v-6.549L64.552,56.66v6.549L63.872,62.831z M62.724,70.23l-23.08-13.333c0,0,0-0.001,0-0.001l-0.71-0.413v-6.542l23.791,13.717  V70.23z M39.433,64.702l24.229,13.991v5.771L39.433,70.949V64.702z M63.599,77.965L39.792,64.218v-6.542L62.873,71.01h0.001  c0,0,0,0.001,0.001,0.001l0.724,0.414V77.965z M63.025,63.14l-4.766-2.748l4.85,2.701L63.025,63.14z M64.846,56.138l-24.223-13.49  l27.5-16.473L92.33,39.696L64.846,56.138z M32.63,38.863v6.553l-0.674-0.394c-0.001-0.001-0.002,0-0.003-0.001  c-0.001,0-0.001-0.001-0.002-0.002l-8.395-4.673l-0.022-0.013c0,0-0.001,0-0.001,0l-0.003-0.002l-15.127-8.43v-0.121v-1v-5.427  L32.63,38.863z M7.542,31.803l0.26,0.151v0.124c0,0.016,0.014,0.024,0.016,0.039c0.005,0.04,0.022,0.068,0.042,0.103  c0.021,0.035,0.037,0.063,0.069,0.087c0.012,0.009,0.013,0.025,0.026,0.033l15.276,8.513c0.001,0,0,0.001,0.001,0.001l8.116,4.695  v6.542L8.267,38.787h0c0,0,0-0.001,0-0.001l-0.724-0.414V31.803z M23.221,55.347l0.011,0.007c0,0,0,0,0,0l8.522,4.93v6.377  l-8.211-4.569l-0.011-0.007c0,0,0,0,0,0l-0.003-0.002L7.526,53.159v-6.57L23.221,55.347z M7.542,53.854l15.69,8.749c0,0,0,0,0,0  l8.116,4.68v6.541L7.542,60.077V53.854z M38.933,71.645l23.791,13.747v6.573L38.933,78.218V71.645z M92.192,61.815l-0.294,0.178  v0.001l-27.7,15.976v-6.547L92.192,55.27V61.815z M63.898,70.904l-0.272-0.156L91.6,54.566l0.3,0.18L63.898,70.904z"
            />
          )}
          {size === 'medium' && (
            <path
              fill="#3BB300"
              d="M93.737,39.235L66.769,29.157l-8.483-3.188L31.3,15.86c-0.08-0.03-0.163-0.045-0.246-0.045  c-0.169,0-0.335,0.062-0.465,0.178L7.98,36.118c-0.149,0.133-0.235,0.322-0.235,0.522v3.098l-0.095,0.06  c-0.197,0.124-0.319,0.338-0.327,0.57l-0.234,7.328l0,7.46c0,0.258,0.142,0.494,0.369,0.616l23.531,12.656l7.388,4.015L61.91,85.101  c0.104,0.056,0.219,0.084,0.332,0.084c0.128,0,0.256-0.035,0.369-0.105l28.312-17.547c0.199-0.123,0.323-0.338,0.33-0.572  l0.234-7.328l0.001-7.46c0-0.258-0.142-0.495-0.369-0.617l-0.807-0.433l3.646-3.256c0.148-0.133,0.233-0.323,0.233-0.522v-7.453  C94.192,39.599,94.011,39.337,93.737,39.235z M93.192,47.21l-4.29,3.832l-7.219,6.438l-10.47,9.321v-6.65L93.192,40.56V47.21z   M90.255,66.771L62.542,83.945v-6.508c0.031-0.007,0.063-0.009,0.093-0.026h0.001c0-0.001,0-0.001,0-0.001l27.619-17.121V66.771z   M31.118,16.86l26.349,9.871L35.365,46.374L9.017,36.533L31.118,16.86z M8.323,47.562v-3.219c0.007,0.003,0.009,0.013,0.017,0.016  l20.98,7.852l1.919,1.046v6.734L8.323,47.649V47.562z M39.245,57.564l22.932,12.334v6.773L39.245,64.321V57.564z M62.467,69.372  L39.545,57.044l0.997-0.618l3.282,1.214c0,0,0,0.001,0.001,0.001l23.413,8.771L62.467,69.372z M68.02,66.062l-23.79-8.911V50.34  l26.385,9.884v6.812L68.02,66.062z M70.85,59.671L44.501,49.8l22.086-19.643l26.334,9.841L70.85,59.671z M35.13,46.928v6.798  l-3.142-1.163c0,0,0,0,0,0l-2.437-0.906c0,0,0,0,0,0L8.745,43.87v-3.245v-0.5v-3.052L35.13,46.928z M8.089,48.203l22.932,12.351  v6.757L8.089,54.977V48.203z M39.011,64.875l22.932,12.351v6.757L39.011,71.648V64.875z M90.489,59.439L90.4,59.493  c0,0,0,0.001-0.001,0.002c-0.001,0-0.002-0.001-0.003,0l-27.62,17.122v-6.731l5.17-3.209l2.862,1.073  c0.035,0.013,0.07,0.019,0.105,0.019c0.068,0,0.131-0.028,0.184-0.069c0.004-0.005,0.012-0.003,0.016-0.007l10.949-9.747  l8.427-5.234V59.439z M84.671,55.618l4.474-3.989l1.045,0.562L84.671,55.618z"
            />
          )}
          {size === 'small' && (
            <path
              fill="#3BB300"
              d="M93.631,50.421l-27.047-11.75l-7.718-3.375L31.803,23.515c-0.09-0.039-0.185-0.059-0.279-0.059  c-0.151,0-0.302,0.05-0.426,0.145L7.223,41.913c-0.173,0.133-0.274,0.338-0.274,0.556v7.094c0,0.278,0.165,0.53,0.421,0.642  l27.047,11.781l7.735,3.359l27.046,11.781c0.09,0.039,0.185,0.059,0.279,0.059c0.151,0,0.302-0.05,0.426-0.145l23.875-18.312  c0.173-0.133,0.274-0.338,0.274-0.556v-7.109C94.052,50.784,93.887,50.531,93.631,50.421z M93.052,58.023L69.776,75.876v-6.368  l23.275-17.837V58.023z M31.565,24.502L57.998,36.01L34.653,53.873L8.236,42.396L31.565,24.502z M7.948,42.927l26.447,11.489v6.47  L7.948,49.365V42.927z M66.347,39.658l26.417,11.477L69.435,69.014L43.018,57.521L66.347,39.658z M42.729,64.506v-6.454  l26.447,11.505v6.47L42.729,64.506z"
            />
          )}
        </svg>
      </g>

      <circle
        cx="600"
        cy="600"
        r="600"
        fillOpacity="0"
        onClick={() => {
          const newInvestment = { ...investment };
          if (investment.smallStack === inventorUid)
            newInvestment.smallStack = '';
          if (investment.mediumStack === inventorUid)
            newInvestment.mediumStack = '';
          if (investment.bigStack === inventorUid) newInvestment.bigStack = '';
          if (size === 'small') {
            newInvestment.smallStack = inventorUid;
          } else if (size === 'medium') {
            newInvestment.mediumStack = inventorUid;
          } else if (size === 'big') {
            newInvestment.bigStack = inventorUid;
          }
          setInvestment(newInvestment);
        }}
      />
    </svg>
  );

  const getInventions = () => {
    const inventionEntries =
      currentInventions! &&
      Object.entries(currentInventions)
        .sort((a, b) => a[0].localeCompare(b[0]))
        .map(([playerUid, invention]) => (
          <>
            {playerUid !== userUid ? (
              <Grid item xs={12} md={6}>
                <Typography variant="h6" align="center">
                  Problem: <b>{currentProblems[invention.problemFounderUid]}</b>{' '}
                  <br />
                  Invention: <b>{invention.name}</b> <br />
                  Tagline: <b>{invention.tagline}</b>
                </Typography>
                <Grid container alignItems="center" justify="center" xs={12}>
                  {stack(playerUid, 'medium')}
                  {stack(playerUid, 'big')}
                  {stack(playerUid, 'small')}
                </Grid>
              </Grid>
            ) : (
              <></>
            )}
          </>
        ));
    return (
      <Grid container alignItems="center" justify="center" xs={12}>
        {inventionEntries}
      </Grid>
    );
  };

  const getContent = () => {
    return (
      <>
        <Grid item xs={12}>
          <Typography variant="h4" align="center">
            Invest your money stacks:
          </Typography>
        </Grid>
        {getInventions()}

        <Grid item>
          <Button
            variant="contained"
            color="primary"
            onClick={onValidateInvestment}
            disabled={
              (investment.bigStack.length !== 0 ? 1 : 0) +
                (investment.mediumStack.length !== 0 ? 1 : 0) +
                (investment.smallStack.length !== 0 ? 1 : 0) <
              Math.min(Object.keys(currentInventions).length - 1, 3)
            }
          >
            {validated ? 'Validated!' : 'Validate'}
          </Button>
        </Grid>
      </>
    );
  };

  return (
    <Grid container spacing={3} alignItems="center" justify="center">
      {validated ? getValidations() : getContent()}
    </Grid>
  );
};

export default Invest;
