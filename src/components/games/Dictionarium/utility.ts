const randomElement = <T>(items: readonly T[]): T => {
  const len = items.length;
  const rand = Math.floor(len * Math.random());
  return items[rand];
};

export default randomElement;
