import React from 'react';
import { AppBar, Link, Toolbar, Typography } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import Timer from '../common/Timer';

const Topbar: React.FC = () => {
  return (
    <AppBar position="fixed">
      <Toolbar>
        <Link component={RouterLink} to="/" underline="none" color="inherit">
          <Typography variant="h6" style={{ flex: 1 }}>
            English Battle Royale
          </Typography>
        </Link>
        <Typography variant="h6" align="right" style={{ flex: 1 }}>
          <Timer />
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Topbar;
