import React, { useEffect } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { createStyles, makeStyles, Typography } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
import { useDocumentData } from 'react-firebase-hooks/firestore';
import { deleteRoom, startRoom } from '../../collection/utility';
import { getFirestore } from '../../config/firebase';
import { CollectionRoom } from '../../collection/types';
import RoomUsers from './RoomUsers';

const useStyles = makeStyles(() =>
  createStyles({
    center: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    startButton: {
      background: 'linear-gradient(45deg, #7b4397 30%, #dc2430 90%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(33, 203, 243, .3)',
      color: 'white',
      height: 48,
      padding: '0 30px',
      margin: 8,
    },
    cancelButton: {
      background: 'linear-gradient(45deg, #4568dc 30%, #b06ab3 90%)',
      border: 0,
      borderRadius: 3,
      boxShadow: '0 3px 5px 2px rgba(33, 203, 243, .3)',
      color: 'white',
      height: 48,
      padding: '0 30px',
      margin: 8,
    },
    roomCode: {
      fontSize: '4em',
      fontWeight: 'bolder',
      display: 'flex',
      justifyContent: 'center',
    },
  })
);

interface HomeProps {
  values: {
    username: string;
    hostCode: string;
    roomCode: string;
    usernameTaken: boolean;
    invalidCode: boolean;
    hosting: boolean;
    joining: boolean;
  };
  setValues: React.Dispatch<
    React.SetStateAction<{
      username: string;
      hostCode: string;
      roomCode: string;
      usernameTaken: boolean;
      invalidCode: boolean;
      hosting: boolean;
      joining: boolean;
    }>
  >;
}

const db = getFirestore();
const roomsCollection = db.collection('rooms');

const HostDialog: React.FC<HomeProps> = ({ values, setValues }) => {
  const classes = useStyles();
  const history = useHistory();

  const roomsQuery = roomsCollection.doc(values.hostCode || '-1');
  const [room] = useDocumentData<CollectionRoom>(roomsQuery);

  useEffect(() => {
    if (room?.status === 'ingame') {
      history.push(`/room/${values.hostCode}`);
    }
  }, [history, room?.status, values.hostCode]);

  if (room == null) return null;

  const handleStart = async () => {
    await startRoom(values.hostCode);
  };

  const handleClose = async () => {
    await deleteRoom(values.hostCode);
    setValues({
      ...values,
      hosting: false,
      hostCode: '',
    });
  };

  const usersInfo = Object.entries(room.users)
    .sort(([, a], [, b]) => a.name.localeCompare(b.name))
    .map(([uid, user]) => ({
      name: user.name,
      uid,
      owner: uid === room.owner,
    }));

  return (
    <div>
      <Dialog
        open={values.hosting}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        disableBackdropClick
        disableEscapeKeyDown
      >
        <DialogTitle id="alert-dialog-title">
          <Typography variant="h5">Waiting for players to join</Typography>
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <span>Share your room ID: </span>
            <br />
            <span className={classes.roomCode}>{values.hostCode}</span>
            <br />
            Current players:
          </DialogContentText>
          <DialogContent>
            <RoomUsers users={usersInfo} />
          </DialogContent>
        </DialogContent>
        <DialogActions className={classes.center}>
          <Button
            className={classes.startButton}
            onClick={handleStart}
            disabled={Object.values(room.users).length < 2}
          >
            Start
          </Button>
          <Button className={classes.cancelButton} onClick={handleClose}>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default HostDialog;
