import React, { useState, useEffect } from 'react';

const Timer = () => {
  const [seconds, setSeconds] = useState(0);
  const [isEnabled, setIsEnabled] = useState(false);

  // eslint-disable-next-line no-unused-vars
  function enable() {
    setIsEnabled(true);
  }

  // eslint-disable-next-line no-unused-vars
  function disable() {
    setIsEnabled(true);
  }

  // eslint-disable-next-line no-unused-vars
  function set(value: number, activate = true) {
    setSeconds(value);
    setIsEnabled(activate);
  }

  useEffect(() => {
    let interval: NodeJS.Timeout | undefined;
    if (isEnabled) {
      interval = setInterval(() => {
        setSeconds((s) => s - 1);
      }, 1000);
    }
    if (seconds === 0) {
      if (interval) {
        clearInterval(interval);
      }
      // TODO add callback call here
    }
    return () => {
      if (interval) {
        clearInterval(interval);
      }
    };
  }, [isEnabled, seconds]);

  return (
    <div>
      {isEnabled && seconds > 0 && <div>Time left: {seconds}s</div>}
      {isEnabled && seconds <= 0 && <div>Timer&apos;s up!</div>}
    </div>
  );
};

export default Timer;
